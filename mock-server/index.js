"use strict";

const express = require("express");
const app = express();
const { v4: uuidv4 } = require("uuid");
const cors = require("cors");

const port = 3001;

app.use(cors());
app.options("*", cors());

app.use(express.json()); // for parsing application/json

app.get("/", (req, res) => res.json({ status: "ok" }));

// Hardcoded servers ip
const servers = [
  {
    ip: "1.1.1.1",
  },
  {
    ip: "2.2.2.2",
  },
  {
    ip: "3.3.3.3",
    credentialsRequired: true,
    username: "hello",
    password: "world",
  },
];

// Hardcoded server groups details
const serverGroups = [
  {
    id: "server-group-001",
    name: "London",
  },
  {
    id: "server-group-002",
    name: "Chicago",
  },
  {
    id: "server-group-003",
    name: "Oslo",
  },
];

// Mock in-memory database for new servers;
const newServers = {};

const router = express.Router();

router.get("/serverGroups", (req, res) => {
  res.json(serverGroups);
});

router.post("/connect", (req, res) => {
  const { ip, username, password } = req.body;

  const server = servers.find((server) => server.ip === ip);

  if (!server) {
    res.status(404).json({ message: `Cannot connect to server ${ip}` });
    return;
  }

  if (
    server.credentialsRequired &&
    (username !== server.username || password !== server.password)
  ) {
    res.status(401).json({ message: "Credential required" });
    return;
  }

  const serverUUID = uuidv4();
  newServers[serverUUID] = { ip };
  res.status(201).json({ id: serverUUID });
});

router.put("/server/:id", (req, res) => {
  const { id } = req.params;
  const { name, serverGroupId } = req.body;

  if (!newServers[id]) {
    res.status(404).json({ message: `server with id ${id} not found` });
    return;
  }

  if (!serverGroups.find((serverGroup) => serverGroupId === serverGroup.id)) {
    res.status(400).json({
      message: `serverGroup with id ${serverGroupId} not found`,
    });
    return;
  }

  if (!name) {
    res.status(400).json({ message: "name is required" });
    return;
  }

  newServers[id].serverGroup = id;

  res.status(200).json({ message: "ok" });
});

app.use("/api/v1", (req, res, next) => {
  // fake delay
  setTimeout(() => {
    router(req, res, next);
  }, 2000);
});

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
