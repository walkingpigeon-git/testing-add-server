# Add server test

## mock server

Mock server codes are located in `mock-server` folder.

To run the mock server:
```
cd mock-server
yarn install
yarn start
```

You should able to access the API at http://localhost:3001

All APIs are delayed by 2000 ms to fake network delay.

I.P. available to connect without credentials:
- 1.1.1.1
- 2.2.2.2


I.P. available to connect with credentials:
- 3.3.3.3
```
username: hello
password: world
```

Connect to other I.P. will return 'Cannot connect to server.' error.

## Frontend

Bootstrapped with create-react-app typescript. API URL can be changed in `.env` file, it is defaulted to `http://localhost:3001/api/v1`

The App used `react-router` to manage dialog flow and `react-redux` to manage state.

`src/components/ConnectServerDialog`, `src/components/UpdateServerDialog`, `src/components/UpdateSuccessDialog` are pure U.I. components taking different props and handlers. The unit tests are also stored in the same folder with `.test.tsx` filename suffix. The unit tests make use of snapshot testing technique and save only the snapshot differences between the original and the changed snapshot when the input props are changed. The snapshots are saved in `__snapshots__` folder.

`src/components/*Page` components are the components to connect redux stores and actions to its Dialog pure U.I. components. Due to timing issue I didn't provide tests for these components.

`store/reducers/*` - Store structures and reducers.

`store/actions/*` - Sync and async actions to be dispatched to the store. Async actions are basically redux-thunk middlewares handling API calls to server.

`store/tests/*` - Unit tests for sync and async actions and the reducers. `fetch` is mocked using `jest-mock-fetch` and the tests dispatch sync and async actions and try to simulate different situations and see how the state in the store changes. The differences in store between initial state and new state after events are dispatched are then captured and saved in `__snapshots__` folders.

To start the frontend:
```
cd my-app
yarn install
yarn start
```

The frontend should be available at http://localhost:3000

The first dialog, you can use i.p. address provided by the mock server to test three different scenarios:
- I.P. without credential: I.P.: 1.1.1.1, 2.2.2.2
- I.P. with credential required: I.P.: 3.3.3.3, username: hello, password: world
- Connection error: anything else

The second screen is to add server to group, to test how it handle 400 error, you can:
- Provide empty name.
- Do not select any server group.

To test server UUID 404 error, you change the URL to an invalid server ID i.e. http://localhost:3000/servers/00000000-0000-0000-0000-000000000000, then click update.


To run test:
```
yarn test
```
