import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import UpdateServerDialog from "../UpdateServerDialog/UpdateServerDialog";
import { RootState } from "../../store/reducers";
import { State as UpdateServerState } from "../../store/reducers/updateServer";
import { State as GetServerGroupsState } from "../../store/reducers/getServerGroups";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";
import * as actions from "../../store/actions/updateServer";
import * as getServerGroupsAction from "../../store/actions/getServerGroups";
import UpdateSuccessDialog from "../UpdateSuccessDialog/UpdateSuccessDialog";

const UpdateServerPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  // on component mount, get list of server groups
  useEffect(() => {
    dispatch(getServerGroupsAction.getServerGroupsRequest());
    // clear update server store on component unmount
    return () => {
      dispatch(actions.updateServerReset());
    };
  }, [dispatch]);

  const { serverUUID } = useParams<{ serverUUID: string }>();
  const [name, setName] = useState("");
  const [serverGroupId, setServerGroupId] = useState("");

  const updateServerState = useSelector<RootState, UpdateServerState>(
    (state) => state.updateServer
  );

  const getServerGroupsState = useSelector<RootState, GetServerGroupsState>(
    (state) => state.getServerGroups
  );

  const onUpdateClickHandler = () => {
    dispatch(actions.updateServerRequest({ name, serverUUID, serverGroupId }));
  };

  const onBackClickHandler = () => {
    history.push("/");
  };

  return updateServerState.success ? (
    <UpdateSuccessDialog name={name} onBackClickHandler={onBackClickHandler} />
  ) : (
    <UpdateServerDialog
      getServerGroupsLoading={getServerGroupsState.loading}
      getServerGroupsSuccess={getServerGroupsState.success}
      getServerGroupsFailure={getServerGroupsState.failure}
      serverGroups={getServerGroupsState.data}
      updateLoading={updateServerState.loading}
      updateFailure={updateServerState.failure}
      updateFailureMessage={updateServerState.failureMessage}
      onUpdateClickHandler={onUpdateClickHandler}
      onBackClickHandler={onBackClickHandler}
      onNameChangeHandler={(e) => setName(e.target.value)}
      onServerGroupChangeHandler={(e) => setServerGroupId(e.target.value)}
    />
  );
};

export default UpdateServerPage;
