import React from "react";
import { render, fireEvent } from "@testing-library/react";
import UpdateServerDialog from "./UpdateServerDialog";

describe("UpdateServerDialog", () => {
  let firstRender: DocumentFragment;
  const props = {
    getServerGroupsLoading: false,
    getServerGroupsSuccess: false,
    getServerGroupsFailure: false,
    updateLoading: false,
    updateFailure: false,
    onUpdateClickHandler: () => {},
    onBackClickHandler: () => {},
    onNameChangeHandler: () => {},
    onServerGroupChangeHandler: () => {},
  };

  beforeAll(() => {
    // base snapshot
    const { asFragment } = render(<UpdateServerDialog {...props} />);
    firstRender = asFragment();
  });

  it("should render", () => {
    expect(firstRender).toMatchSnapshot();
  });

  it("should disable select dropdown and show loading message when getServerGroupsLoading=true", () => {
    const { asFragment } = render(
      <UpdateServerDialog {...props} getServerGroupsLoading />
    );
    expect(firstRender).toMatchDiffSnapshot(asFragment());
  });

  it("should fill select dropdown with data from serverGroups prop when getServerGroupsSuccess=true", () => {
    const { asFragment } = render(
      <UpdateServerDialog
        {...props}
        getServerGroupsSuccess
        serverGroups={[{ name: "group1", id: "id-1" }]}
      />
    );
    expect(firstRender).toMatchDiffSnapshot(asFragment());
  });

  it("should show error message when getServerGroupsFailure=true", () => {
    const { asFragment } = render(
      <UpdateServerDialog {...props} getServerGroupsFailure />
    );
    expect(firstRender).toMatchDiffSnapshot(asFragment());
  });

  it("should disable Update button and change label to Updating when updateLoading=true", () => {
    const { asFragment } = render(
      <UpdateServerDialog {...props} updateLoading />
    );
    expect(firstRender).toMatchDiffSnapshot(asFragment());
  });

  it("should show updateFailureMessage message when updateFailure=true", () => {
    const { asFragment } = render(
      <UpdateServerDialog
        {...props}
        updateFailure
        updateFailureMessage="update failed"
      />
    );
    expect(firstRender).toMatchDiffSnapshot(asFragment());
  });

  it("should trigger onNameChangeHandler when name input is changed", () => {
    const mockFn = jest.fn();

    const { getByLabelText } = render(
      <UpdateServerDialog {...props} onNameChangeHandler={mockFn} />
    );

    const input = getByLabelText("Name:");
    fireEvent.change(input, { target: { value: "a" } });
    expect(mockFn).toBeCalledTimes(1);
  });

  it("should trigger onServerGroupChangeHandler when name input is changed", () => {
    const mockFn = jest.fn();

    const { getByLabelText } = render(
      <UpdateServerDialog {...props} onServerGroupChangeHandler={mockFn} />
    );

    const input = getByLabelText("Server Groups:");
    fireEvent.change(input, { target: { value: "a" } });
    expect(mockFn).toBeCalledTimes(1);
  });

  it("should trigger onBackClickHandler when Back button is clicked", () => {
    const mockFn = jest.fn();

    const { getByText } = render(
      <UpdateServerDialog {...props} onBackClickHandler={mockFn} />
    );

    fireEvent.click(getByText("Back"));
    expect(mockFn).toBeCalledTimes(1);
  });

  it("should trigger onUpdateClickHandler when Back button is clicked", () => {
    const mockFn = jest.fn();

    const { getByText } = render(
      <UpdateServerDialog {...props} onUpdateClickHandler={mockFn} />
    );

    fireEvent.click(getByText("Update"));
    expect(mockFn).toBeCalledTimes(1);
  });
});
