import React from "react";
import css from "./UpdateSuccessDialog.module.css";

type Props = {
  name: string;
  onBackClickHandler: () => void;
};

const UpdateSuccessDialog = ({ name, onBackClickHandler }: Props) => (
  <div className={css.container}>
    Server {name} added successfully!
    <div>
      <button type="button" onClick={onBackClickHandler}>
        Back to home
      </button>
    </div>
  </div>
);

export default UpdateSuccessDialog;
