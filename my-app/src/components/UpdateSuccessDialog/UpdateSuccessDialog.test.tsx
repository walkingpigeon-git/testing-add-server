import React from "react";
import { render, fireEvent } from "@testing-library/react";
import UpdateSuccessDialog from "./UpdateSuccessDialog";

describe("UpdateSuccessDialog", () => {
  let firstRender: DocumentFragment;
  const props = {
    name: '',
    onBackClickHandler: () => {}
  };

  beforeAll(() => {
    // base snapshot
    const { asFragment } = render(<UpdateSuccessDialog {...props} />);
    firstRender = asFragment();
  });

  it("should render", () => {
    expect(firstRender).toMatchSnapshot();
  });

  it("should show name in Dialog when name prop is given", () => {
    const { asFragment } = render(
      <UpdateSuccessDialog {...props} name="my new server" />
    );
    expect(firstRender).toMatchDiffSnapshot(asFragment());
  });

  it("should trigger onBackClickHandler when Back to home button is clicked", () => {
    const mockFn = jest.fn();

    const { getByText } = render(
      <UpdateSuccessDialog {...props} onBackClickHandler={mockFn} />
    );

    fireEvent.click(getByText("Back to home"));
    expect(mockFn).toBeCalledTimes(1);
  });
});
