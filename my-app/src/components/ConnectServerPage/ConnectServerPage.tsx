import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import ConnectServerDialog from "../ConnectServerDialog/ConnectServerDialog";
import { RootState } from "../../store/reducers";
import { State as ConnectServerState } from "../../store/reducers/connectServer";
import * as actions from "../../store/actions/connectServer";

const ConnectServerPage = () => {
  const [ip, setIp] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const {
    loading,
    failure,
    success,
    credentialRequired,
    serverUUID,
  } = useSelector<RootState, ConnectServerState>(
    (state) => state.connectServer
  );

  const history = useHistory();
  const dispatch = useDispatch();

  // redirect to add server to server group page on connect success
  useEffect(() => {
    if (!success && !serverUUID) {
      return;
    }
    history.push(`/servers/${serverUUID}`);

    // clear store on component unmount
    return () => {
      // clear connect server store
      dispatch(actions.connectServerReset());
    };
  }, [history, dispatch, success, serverUUID]);

  const onConnectClickHandler = () => {
    dispatch(actions.connectServerRequest({ ip, username, password }));
  };

  const onBackClickHandler = () => {
    setUsername("");
    setPassword("");
    dispatch(actions.connectServerReset());
  };

  return (
    <ConnectServerDialog
      loading={loading}
      failure={failure}
      credentialRequired={
        credentialRequired || username !== "" || password !== ""
      }
      onBackClickHandler={onBackClickHandler}
      onConnectClickHandler={onConnectClickHandler}
      onIpChangeHandler={(e) => setIp(e.target.value)}
      onUsernameChangeHandler={(e) => setUsername(e.target.value)}
      onPasswordChangeHandler={(e) => setPassword(e.target.value)}
    />
  );
};

export default ConnectServerPage;
