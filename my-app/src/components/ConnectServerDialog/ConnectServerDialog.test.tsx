import React from "react";
import { render, fireEvent } from "@testing-library/react";
import ConnectServerDialog from "./ConnectServerDialog";

describe("ConnectServerDialog", () => {
  let firstRender: DocumentFragment;
  const props = {
    loading: false,
    failure: false,
    credentialRequired: false,
    onBackClickHandler: () => {},
    onConnectClickHandler: () => {},
    onIpChangeHandler: () => {},
    onUsernameChangeHandler: () => {},
    onPasswordChangeHandler: () => {},
  };

  beforeAll(() => {
    // base snapshot
    const { asFragment } = render(<ConnectServerDialog {...props} />);
    firstRender = asFragment();
  });

  it("should render", () => {
    expect(firstRender).toMatchSnapshot();
  });

  it("should disable inputs and change button label from 'connect' to 'connecting' when loading=true", () => {
    const { asFragment } = render(<ConnectServerDialog {...props} loading />);
    expect(firstRender).toMatchDiffSnapshot(asFragment());
  });

  it("should display error message when failure=true", () => {
    const { asFragment } = render(<ConnectServerDialog {...props} failure />);
    expect(firstRender).toMatchDiffSnapshot(asFragment());
  });

  it("should display user and password inputs, back button and credential required message when credentialRequired=true", () => {
    const { asFragment } = render(
      <ConnectServerDialog {...props} credentialRequired />
    );
    expect(firstRender).toMatchDiffSnapshot(asFragment());
  });

  it("should trigger onConnectClickHandler when Connect button is clicked", () => {
    const mockFn = jest.fn();

    const { getByText } = render(
      <ConnectServerDialog {...props} onConnectClickHandler={mockFn} />
    );

    fireEvent.click(getByText("Connect"));
    expect(mockFn).toBeCalledTimes(1);
  });

  it("should trigger onBackClickHandler when Back button is clicked", () => {
    const mockFn = jest.fn();

    const { getByText } = render(
      <ConnectServerDialog
        {...props}
        onBackClickHandler={mockFn}
        credentialRequired
      />
    );

    fireEvent.click(getByText("Back"));
    expect(mockFn).toBeCalledTimes(1);
  });

  it("should trigger onUsernameChangeHandler when username input is changed", () => {
    const mockFn = jest.fn();

    const { getByLabelText } = render(
      <ConnectServerDialog
        {...props}
        onUsernameChangeHandler={mockFn}
        credentialRequired
      />
    );

    const input = getByLabelText("Username:");
    fireEvent.change(input, { target: { value: "a" } });
    expect(mockFn).toBeCalledTimes(1);
  });

  it("should trigger onPasswordChangeHandler when password input is changed", () => {
    const mockFn = jest.fn();

    const { getByLabelText } = render(
      <ConnectServerDialog
        {...props}
        onPasswordChangeHandler={mockFn}
        credentialRequired
      />
    );

    const input = getByLabelText("Password:");
    fireEvent.change(input, { target: { value: "a" } });
    expect(mockFn).toBeCalledTimes(1);
  });

  it("should trigger onIpChangeHandler when I.P. input is changed", () => {
    const mockFn = jest.fn();

    const { getByLabelText } = render(
      <ConnectServerDialog
        {...props}
        onIpChangeHandler={mockFn}
        credentialRequired
      />
    );

    const input = getByLabelText("I.P.:");
    fireEvent.change(input, { target: { value: "a" } });
    expect(mockFn).toBeCalledTimes(1);
  });


});
