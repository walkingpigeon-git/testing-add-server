import React from "react";
import css from "./ConnectServerDialog.module.css";

type Props = {
  credentialRequired?: boolean;
  loading?: boolean;
  failure?: boolean;
  serverUUID?: string;
  onConnectClickHandler: () => void;
  onBackClickHandler: () => void;
  onIpChangeHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onUsernameChangeHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onPasswordChangeHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const ConnectServerDialog = ({
  loading,
  failure,
  credentialRequired,
  onConnectClickHandler,
  onBackClickHandler,
  onIpChangeHandler,
  onUsernameChangeHandler,
  onPasswordChangeHandler,
}: Props) => {
  return (
    <div className={css.container}>
      <div className={css.row}>
        <label>
          I.P.:
          <input
            type="text"
            disabled={credentialRequired}
            onChange={onIpChangeHandler}
          />
        </label>
      </div>
      {credentialRequired ? (
        <>
          <div className={css.row}>
            <label>
              Username:
              <input type="text" onChange={onUsernameChangeHandler} />
            </label>
          </div>
          <div className={css.row}>
            <label>
              Password:
              <input type="text" onChange={onPasswordChangeHandler} />
            </label>
          </div>
        </>
      ) : null}

      <div className={css.row}>
        {credentialRequired ? (
          <button type="button" onClick={onBackClickHandler} disabled={loading}>
            Back
          </button>
        ) : null}
        <button
          type="submit"
          onClick={onConnectClickHandler}
          disabled={loading}
        >
          {loading ? "Connecting..." : "Connect"}
        </button>
      </div>

      <div>
        {credentialRequired
          ? "Credential is required for this server. Please provide correct username and password."
          : null}
      </div>
      <div>{failure ? "Cannot connect to server." : null}</div>
    </div>
  );
};

export default ConnectServerDialog;
