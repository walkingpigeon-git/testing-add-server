import {
  CONNECT_SERVER_STARTED,
  CONNECT_SERVER_SUCCESS,
  CONNECT_SERVER_FAILURE,
  CONNECT_SERVER_RESET,
  actionType,
} from "../types/connectServer";

const { REACT_APP_API_ENDPOINT_URI } = process.env;

type ActionType = {
  type: actionType;
};

type SuccessPayload = {
  serverUUID: string;
};

type FailurePayload = {
  credentialRequired?: boolean;
};

type requestPayload = {
  ip: string;
  username?: string;
  password?: string;
};

export type SuccessActionType = ActionType & {
  payload: SuccessPayload;
};

export type FailureActionType = ActionType & {
  payload?: FailurePayload;
};

export const connectServerStarted = (): ActionType => ({
  type: CONNECT_SERVER_STARTED,
});

export const connectServerSuccess = ({
  serverUUID,
}: SuccessPayload): SuccessActionType => ({
  type: CONNECT_SERVER_SUCCESS,
  payload: {
    serverUUID,
  },
});

export const connectServerFailure = (
  payload?: FailurePayload
): FailureActionType => ({
  type: CONNECT_SERVER_FAILURE,
  ...(payload?.credentialRequired
    ? { payload: { credentialRequired: payload.credentialRequired } }
    : {}),
});

export const connectServerReset = (): ActionType => ({
  type: CONNECT_SERVER_RESET,
});

export const connectServerRequest = (payload: requestPayload) => async (
  dispatch: (
    actionType: ActionType | SuccessActionType | FailureActionType
  ) => void
) => {
  dispatch(connectServerStarted());

  try {
    const url = `${REACT_APP_API_ENDPOINT_URI}/connect`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    });

    if (response.ok) {
      const data = await response.json();
      dispatch(connectServerSuccess({ serverUUID: data.id }));
      return;
    }

    if (response.status === 401) {
      dispatch(connectServerFailure({ credentialRequired: true }));
      return;
    }
    dispatch(connectServerFailure());
  } catch {
    // network error
    dispatch(connectServerFailure());
  }
};
