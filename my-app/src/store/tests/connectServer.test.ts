import connectServerReducer, { State } from "../reducers/connectServer";
import { connectServerRequest, connectServerReset } from "../actions/connectServer";

import createMockStore from "./testUtils/createMockStore";
import nextTick from "./testUtils/nextTick";


describe("connectServer request", () => {
  let initState: State;
  let store: any;

  beforeAll(() => {
    initState = connectServerReducer(undefined, {});
  });

  beforeEach(() => {
    store = createMockStore();
  });

  it("should return the initial state with no action", () => {
    expect(initState).toMatchSnapshot();
  });

  it("should pass correct parameters to fetch API", () => {
    fetchMock.mockResponseOnce("{}");
    store.dispatch(connectServerRequest({ ip: "1.1.1.1", username: 'user', password: '1234' }));

    expect(fetchMock.mock.calls).toMatchSnapshot();
  });

  it("should return state with loading=true when request started", async () => {
    fetchMock.mockResponseOnce("{}");

    store.dispatch(connectServerRequest({ ip: "1.1.1.1" }));
    expect(initState).toMatchDiffSnapshot(store.getState().connectServer);
  });

  it("should return state with success=true and serverUUID when request succeed with HTTP 201", async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify({
        id: "123e4567-e89b-12d3-a456-426652340000",
      }),
      { status: 201 }
    );

    store.dispatch(connectServerRequest({ ip: "1.1.1.1" }));
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().connectServer);
  });

  it("should return state with credentialRequired=true when request failed with 401", async () => {
    fetchMock.mockResponseOnce("{}", { status: 401 });

    store.dispatch(connectServerRequest({ ip: "1.1.1.1" }));
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().connectServer);
  });

  it("should return state with failure=true when request failed with 40x", async () => {
    fetchMock.mockResponseOnce("{}", { status: 400 });

    store.dispatch(connectServerRequest({ ip: "1.1.1.1" }));
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().connectServer);
  });

  it("should return state with failure=true when network error", async () => {
    fetchMock.mockAbortOnce();

    store.dispatch(connectServerRequest({ ip: "1.1.1.1" }));
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().connectServer);
  });

  it("should return state to initState when connectServerReset is dispatched", () => {
    const resetState = connectServerReducer(undefined, connectServerReset());
    expect(initState).toEqual(resetState);
  });
});
