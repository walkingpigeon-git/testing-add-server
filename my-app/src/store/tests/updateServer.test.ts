import updateServerReducer, { State } from "../reducers/updateServer";
import {
  updateServerRequest,
  updateServerReset,
} from "../actions/updateServer";

import createMockStore from "./testUtils/createMockStore";
import nextTick from "./testUtils/nextTick";

describe("updateServer request", () => {
  let initState: State;
  let store: any;

  beforeAll(() => {
    initState = updateServerReducer(undefined, {});
  });

  beforeEach(() => {
    store = createMockStore();
  });

  it("should return the initial state with no action", () => {
    expect(initState).toMatchSnapshot();
  });

  it("should pass correct parameters to fetch API", () => {
    fetchMock.mockResponseOnce("{}");
    store.dispatch(
      updateServerRequest({
        name: "name",
        serverGroupId: "id-1",
        serverUUID: "1",
      })
    );

    expect(fetchMock.mock.calls).toMatchSnapshot();
  });

  it("should return state with loading=true when request started", async () => {
    fetchMock.mockResponseOnce("{}");

    store.dispatch(
      updateServerRequest({
        name: "name",
        serverGroupId: "id-1",
        serverUUID: "1",
      })
    );
    expect(initState).toMatchDiffSnapshot(store.getState().updateServer);
  });

  it("should return state with success=true when request succeed with HTTP 200", async () => {
    fetchMock.mockResponseOnce("{}", { status: 200 });

    store.dispatch(
      updateServerRequest({
        name: "name",
        serverGroupId: "id-1",
        serverUUID: "1",
      })
    );
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().updateServer);
  });

  it("should return state with failure=true and failureMessage is filled when request failed with 400", async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify({ message: "error message 400 from server" }),
      { status: 400 }
    );

    store.dispatch(
      updateServerRequest({
        name: "name",
        serverGroupId: "id-1",
        serverUUID: "1",
      })
    );
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().updateServer);
  });

  it("should return state with failure=true and failureMessage is filled when request failed with 404", async () => {
    fetchMock.mockResponseOnce(
      JSON.stringify({ message: "error message 404 from server" }),
      { status: 404 }
    );

    store.dispatch(
      updateServerRequest({
        name: "name",
        serverGroupId: "id-1",
        serverUUID: "1",
      })
    );
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().updateServer);
  });

  it ("should return state with failure=true on network error", async() => {
    fetchMock.mockAbortOnce();

    store.dispatch(
      updateServerRequest({
        name: "name",
        serverGroupId: "id-1",
        serverUUID: "1",
      })
    );
    await nextTick();

    expect(initState).toMatchDiffSnapshot(store.getState().updateServer);
  });

  it("should return state to initState when updateServerReset is dispatched", () => {
    const resetState = updateServerReducer(undefined, updateServerReset());
    expect(initState).toEqual(resetState);
  });
});
