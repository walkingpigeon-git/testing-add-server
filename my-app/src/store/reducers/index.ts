import { combineReducers } from "redux";
import connectServer, { State as ConnectServerState } from "./connectServer";
import getServerGroups, {
  State as GetServerGroupState,
} from "./getServerGroups";
import updateServer, { State as UpdateServerState } from "./updateServer";

export default combineReducers({
  connectServer,
  getServerGroups,
  updateServer,
});

export type RootState = {
  connectServer: ConnectServerState;
  getServerGroups: GetServerGroupState;
  updateServer: UpdateServerState;
};
