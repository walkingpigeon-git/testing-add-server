import {
  CONNECT_SERVER_STARTED,
  CONNECT_SERVER_SUCCESS,
  CONNECT_SERVER_FAILURE,
  CONNECT_SERVER_RESET,
  actionType,
} from "../types/connectServer";

export type State = {
  loading: boolean;
  credentialRequired: boolean;
  success: boolean;
  failure: boolean;
  serverUUID?: string;
};

export type Payload = {
  serverUUID?: string;
  credentialRequired?: boolean;
};

const initialState: State = {
  loading: false,
  credentialRequired: false,
  success: false,
  failure: false,
};

const connectServer = (
  state = initialState,
  {
    type,
    payload,
  }: {
    type?: actionType;
    payload?: Payload;
  }
): State => {
  switch (type) {
    case CONNECT_SERVER_STARTED:
      return {
        ...initialState,
        loading: true,
      };
    case CONNECT_SERVER_SUCCESS: {
      return {
        ...state,
        loading: false,
        success: true,
        serverUUID: payload?.serverUUID,
      };
    }
    case CONNECT_SERVER_FAILURE: {
      return {
        ...state,
        loading: false,
        success: false,
        failure: false,
        ...(payload?.credentialRequired
          ? { credentialRequired: true }
          : { failure: true }),
      };
    }
    case CONNECT_SERVER_RESET:
      return initialState;
    default:
      return state;
  }
};

export default connectServer;
