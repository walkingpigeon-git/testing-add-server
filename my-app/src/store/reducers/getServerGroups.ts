import {
  GET_SERVER_GROUPS_STARTED,
  GET_SERVER_GROUPS_SUCCESS,
  GET_SERVER_GROUPS_FAILURE,
  actionType,
} from "../types/getServerGroups";

type ServerGroup = {
  id: string;
  name: string;
};

export type State = {
  loading: boolean;
  success: boolean;
  failure: boolean;
  data?: ServerGroup[];
};

export type Payload = {
  data?: ServerGroup[];
};

const initialState: State = {
  loading: false,
  success: false,
  failure: false,
};

const getServerGroups = (
  state = initialState,
  {
    type,
    payload,
  }: {
    type?: actionType;
    payload?: Payload;
  }
): State => {
  switch (type) {
    case GET_SERVER_GROUPS_STARTED:
      return {
        ...initialState,
        loading: true,
      };
    case GET_SERVER_GROUPS_SUCCESS: {
      return {
        ...state,
        loading: false,
        success: true,
        failure: false,
        data: payload?.data
      };
    }
    case GET_SERVER_GROUPS_FAILURE:
      {
        return {
          ...state,
          loading: false,
          success: false,
          failure: true,
        };
      }
    default:
      return state;
  }
};

export default getServerGroups;
