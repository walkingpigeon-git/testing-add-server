import {
  UPDATE_SERVER_STARTED,
  UPDATE_SERVER_SUCCESS,
  UPDATE_SERVER_FAILURE,
  UPDATE_SERVER_RESET,
  actionType,
} from "../types/updateServer";

export type State = {
  loading: boolean;
  success: boolean;
  failure: boolean;
  failureMessage?: string
};

const initialState: State = {
  loading: false,
  success: false,
  failure: false,
};

type Payload = {
  failureMessage: string
}

const updateServer = (
  state = initialState,
  {
    type,
    payload,
  }: {
    type?: actionType;
    payload?: Payload;
  }
): State => {
  switch (type) {
    case UPDATE_SERVER_STARTED:
      return {
        ...initialState,
        loading: true,
      };
    case UPDATE_SERVER_SUCCESS: {
      return {
        ...state,
        loading: false,
        success: true,
        failure: false,
      };
    }
    case UPDATE_SERVER_FAILURE: {
      return {
        ...state,
        loading: false,
        success: false,
        failure: true,
        failureMessage: payload?.failureMessage
      };
    }
    case UPDATE_SERVER_RESET:
      return initialState;
    default:
      return state;
  }
};

export default updateServer;
