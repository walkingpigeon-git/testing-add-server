import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import UpdateServerPage from "./components/UpdateServerPage/UpdateServerPage";
import ConnectServerPage from "./components/ConnectServerPage/ConnectServerPage";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/servers/:serverUUID" exact>
          <UpdateServerPage />
        </Route>
        <Route path="/">
          <ConnectServerPage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
